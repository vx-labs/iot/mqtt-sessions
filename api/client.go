package api

import (
	"context"
	"io"

	"github.com/vx-labs/iot-mqtt-events"

	"github.com/vx-labs/iot-mqtt-sessions/types"
	"github.com/vx-labs/mqtt-protocol/packet"
	"google.golang.org/grpc"
)

type Client struct {
	api types.SessionsServiceClient
}

type SessionApplier func(events.Session)

func New(conn *grpc.ClientConn) *Client {
	return &Client{
		api: types.NewSessionsServiceClient(conn),
	}
}

func (c *Client) New(ctx context.Context, id, tenant string, connect *packet.Connect) error {
	session := types.CreateSessionRequest{
		Id:           id,
		Tenant:       tenant,
		ConnectInfos: connect,
	}
	_, err := c.api.New(ctx, &session)
	return err
}

func (c *Client) SetOffline(ctx context.Context, id string) error {
	_, err := c.api.SetOffline(ctx, &events.SessionSelector{
		Id: id,
	})
	return err
}

func (c *Client) Delete(ctx context.Context, id string) error {
	_, err := c.api.Delete(ctx, &events.SessionSelector{
		Id: id,
	})
	return err
}

func (c *Client) UpdateTTL(ctx context.Context, id string) error {
	_, err := c.api.UpdateTTL(ctx, &events.SessionSelector{
		Id: id,
	})
	return err
}

func (c *Client) Get(ctx context.Context, id string) (events.Session, error) {
	req := &events.SessionSelector{Id: id}
	resp, err := c.api.Get(ctx, req)
	if err != nil {
		return events.Session{}, err
	}
	return *resp, nil
}

type SessionFilterOp func(events.SessionSelector) events.SessionSelector

func HasTenant(id string) SessionFilterOp {
	if id == "" {
		return nil
	}
	return func(s events.SessionSelector) events.SessionSelector {
		s.Tenant = id
		return s
	}
}
func HasIDIn(id []string) SessionFilterOp {
	if len(id) == 0 {
		return nil
	}
	return func(s events.SessionSelector) events.SessionSelector {
		s.IdIn = id
		return s
	}
}
func (c *Client) List(ctx context.Context, filters ...SessionFilterOp) (events.SessionList, error) {
	selector := events.SessionSelector{}
	for _, filter := range filters {
		if filter != nil {
			selector = filter(selector)
		}
	}
	stream, err := c.api.List(ctx, &selector)
	if err != nil {
		return nil, err
	}
	set := events.SessionList{}
	for {
		session, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		set = append(set, *session)
	}
	return set, nil
}
