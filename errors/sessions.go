package errors

import "fmt"

var (
	SessionNotFound = fmt.Errorf("session not found")
)
