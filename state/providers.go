package state

import (
	"github.com/vx-labs/iot-mqtt-sessions/state/nats"
)

func NATS(host, cluster string) Provider {
	return nats.NewNATSProvider(host, cluster)
}
