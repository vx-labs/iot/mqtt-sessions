package nats

import (
	"context"
	"errors"
	"log"

	"github.com/golang/protobuf/proto"

	"github.com/google/uuid"
	"github.com/nats-io/go-nats-streaming"
	"github.com/vx-labs/iot-mqtt-events"
	"github.com/vx-labs/iot-mqtt-sessions/store"
	"github.com/vx-labs/iot-mqtt-sessions/types"
)

const natsTopic = "vx.iot.mqtt"

type sessionStore interface {
	ById(id string) (events.Session, error)
	All() (events.SessionList, error)
	Upsert(sess events.Session) error
	Update(id string, transform func(events.Session) (events.Session, error)) error
	Delete(id string) error
	Exists(id string) bool
}

type natsProvider struct {
	store        sessionStore
	nats         stan.Conn
	subscription stan.Subscription
}

func NewNATSProvider(host, cluster string) *natsProvider {
	p := &natsProvider{
		store: store.NewSessionStore(),
	}
	clientID := uuid.New().String()
	sc, err := stan.Connect(cluster, clientID,
		stan.NatsURL(host),
		stan.Pings(5, 3))
	if err != nil {
		panic(err)
	}
	sub, err := sc.Subscribe(natsTopic, func(m *stan.Msg) {
		ev := &events.MQTTEvent{}
		err := proto.Unmarshal(m.Data, ev)
		if err != nil {
			log.Printf("ERR: failed to decode received event: %v", err)
		}
		err = p.Apply(m.Timestamp, ev)
		if err != nil {
			log.Printf("WARN: failed to apply %s event: %v", ev.Name, err)
		}
	}, stan.StartAtSequence(0))
	if err != nil {
		panic(err)
	}
	p.nats = sc
	p.subscription = sub
	return p
}

func (p *natsProvider) Close() error {
	p.subscription.Unsubscribe()
	return p.nats.Close()
}

func (p *natsProvider) Apply(timestamp int64, event *events.MQTTEvent) error {
	switch event.Name {
	case "session_created":
		session := event.GetSessionCreated().Session
		return p.store.Upsert(*session)
	case "session_deleted":
		return p.store.Delete(event.GetSessionDeleted().ID)
	case "session_keepalived":
		return p.store.Update(event.GetSessionKeepalived().ID, func(s events.Session) (events.Session, error) {
			s.LastContact = timestamp
			return s, nil
		})
	case "session_disconnected":
		return p.store.Update(event.GetSessionDisconnected().ID, func(s events.Session) (events.Session, error) {
			s.Online = false
			return s, nil
		})
	case "session_reconnected":
		return p.store.Update(event.GetSessionReconnected().ID, func(s events.Session) (events.Session, error) {
			s.Online = true
			return s, nil
		})
	default:
		return nil
	}
}
func (p *natsProvider) Store(ctx context.Context, req *types.CreateSessionRequest) error {
	pb := events.Session{
		Id:                req.Id,
		Tenant:            req.Tenant,
		Clean:             req.ConnectInfos.Clean,
		Username:          req.ConnectInfos.Username,
		WillPayload:       req.ConnectInfos.WillPayload,
		WillTopic:         req.ConnectInfos.WillTopic,
		WillQoS:           req.ConnectInfos.WillQos,
		WillRetain:        req.ConnectInfos.WillRetain,
		ClientID:          req.ConnectInfos.ClientId,
		KeepaliveInterval: req.ConnectInfos.KeepaliveTimer,
		Online:            true,
	}
	ev := events.MQTTEvent{
		Name: "session_created",
		Event: &events.MQTTEvent_SessionCreated{
			SessionCreated: &events.SessionCreated{
				Session: &pb,
			},
		},
	}
	payload, err := proto.Marshal(&ev)
	if err != nil {
		return err
	}
	return p.nats.Publish(natsTopic, payload)
}
func (p *natsProvider) Get(ctx context.Context, sessionId string) (events.Session, error) {
	return p.store.ById(sessionId)
}
func (p *natsProvider) All(ctx context.Context) (events.SessionList, error) {
	return p.store.All()
}
func (p *natsProvider) Delete(ctx context.Context, id string) error {
	ev := events.MQTTEvent{
		Name: "session_deleted",
		Event: &events.MQTTEvent_SessionDeleted{
			SessionDeleted: &events.SessionDeleted{
				ID: id,
			},
		},
	}
	payload, err := proto.Marshal(&ev)
	if err != nil {
		return err
	}
	return p.nats.Publish(natsTopic, payload)
}
func (p *natsProvider) Status() types.HealthCheckResponse_ServingStatus {
	return types.HealthCheckResponse_SERVING
}
func (p *natsProvider) UpdateSessionTTL(ctx context.Context, id string) error {
	session, err := p.store.ById(id)
	if err != nil || !session.Online {
		return errors.New("session does not exist, or is offline")
	}
	ev := events.MQTTEvent{
		Name: "session_keepalived",
		Event: &events.MQTTEvent_SessionKeepalived{
			SessionKeepalived: &events.SessionKeepalived{
				ID: id,
			},
		},
	}
	payload, err := proto.Marshal(&ev)
	if err != nil {
		return err
	}
	return p.nats.Publish(natsTopic, payload)
}

func (p *natsProvider) UpdateSessionOnlineStatus(ctx context.Context, id string, status bool) error {
	var payload []byte
	var err error
	switch status {
	case true:
		payload, err = proto.Marshal(&events.MQTTEvent{
			Name: "session_reconnected",
			Event: &events.MQTTEvent_SessionReconnected{
				SessionReconnected: &events.SessionReconnected{
					ID: id,
				},
			},
		})
	case false:
		payload, err = proto.Marshal(&events.MQTTEvent{
			Name: "session_disconnected",
			Event: &events.MQTTEvent_SessionDisconnected{
				SessionDisconnected: &events.SessionDisconnected{
					ID: id,
				},
			},
		})
	}
	if err != nil {
		return err
	}
	return p.nats.Publish(natsTopic, payload)
}
