package state

import (
	"io"

	"github.com/vx-labs/iot-mqtt-events"
	"github.com/vx-labs/iot-mqtt-sessions/types"
	"golang.org/x/net/context"
)

type Provider interface {
	io.Closer
	Store(ctx context.Context, req *types.CreateSessionRequest) error
	Get(ctx context.Context, sessionId string) (events.Session, error)
	All(ctx context.Context) (events.SessionList, error)
	Delete(ctx context.Context, sessionId string) error
	Status() types.HealthCheckResponse_ServingStatus
	UpdateSessionTTL(ctx context.Context, id string) error
	UpdateSessionOnlineStatus(ctx context.Context, id string, status bool) error
}
