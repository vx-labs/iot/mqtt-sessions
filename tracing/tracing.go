package tracing

import (
	"io"
	"log"

	"github.com/opentracing/opentracing-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
)

var tracerInstance opentracing.Tracer
var closer io.Closer

func init() {
	cfg, err := jaegercfg.FromEnv()
	if err != nil {
		// parsing errors might happen here, such as when we get a string where we expect a number
		log.Printf("Could not parse Jaeger env vars: %s", err.Error())
		return
	}
	cfg.ServiceName = "mqtt-sessions"
	tracerInstance, closer, err = cfg.NewTracer()
	if err != nil {
		log.Fatalf("Could not initialize jaeger tracer: %s", err.Error())
		return
	}
	log.Println("tracer started")
}

func Instance() opentracing.Tracer {
	return tracerInstance
}

func Close() error {
	defer log.Println("tracer closed")
	return closer.Close()
}
