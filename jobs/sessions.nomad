job "sessions" {
  constraint {
    operator = "distinct_hosts"
    value    = "true"
  }

  datacenters = ["dc1"]
  type        = "service"

  update {
    max_parallel     = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    health_check     = "checks"
    auto_revert      = true
    canary           = 0
  }

  group "sessions-store" {
    count = 1

    restart {
      attempts = 5
      interval = "5m"
      delay    = "15s"
      mode     = "delay"
    }

    ephemeral_disk {
      size = 300
    }

    task "sessions-store" {
      driver = "docker"

      env {
        CONSUL_HTTP_ADDR          = "172.17.0.1:8500"
        ROUTER_HOST               = "172.17.0.1:4141"
        BACKEND                   = "nats"
        NATS_URL                  = "nats://servers.nats.discovery.par1.vx-labs.net:4222"
        JAEGER_SAMPLER_TYPE       = "const"
        JAEGER_SAMPLER_PARAM      = "1"
        JAEGER_REPORTER_LOG_SPANS = "true"
        JAEGER_AGENT_HOST         = "${NOMAD_IP_health}"
      }

      config {
        force_pull = true
        image      = "quay.io/vxlabs/iot-mqtt-sessions:v2.0.3"

        port_map {
          SessionsService = 7996
          health          = 9000
        }
      }

      resources {
        cpu    = 500
        memory = 128

        network {
          mbits = 10
          port  "SessionsService"{}
          port  "health"{}
        }
      }

      service {
        name = "SessionsService"
        port = "SessionsService"
        tags = ["leader"]

        check {
          type     = "http"
          path     = "/health"
          port     = "health"
          interval = "5s"
          timeout  = "2s"
        }
      }
    }
  }
}
