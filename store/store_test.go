package store

import (
	"testing"

	"github.com/stretchr/testify/assert"
	types "github.com/vx-labs/iot-mqtt-events"
)

const (
	sessionID = "cb8f3900-4146-4499-a880-c01611a6d9ee"
)

func TestSessionStore(t *testing.T) {
	store := NewSessionStore()

	t.Run("create", func(t *testing.T) {
		err := store.Upsert(types.Session{
			Id:        sessionID,
			WillTopic: []byte("devices"),
		})
		assert.Nil(t, err)
	})

	t.Run("lookup", lookup(store, sessionID))
	t.Run("All", func(t *testing.T) {
		set, err := store.All()
		assert.Nil(t, err)
		assert.Equal(t, 1, len(set))
	})
	t.Run("update", func(t *testing.T) {
		err := store.Update(sessionID, func(sess types.Session) (types.Session, error) {
			sess.Tenant = "tenant1"
			return sess, nil
		})
		assert.Nil(t, err)
		s, err := store.ById(sessionID)
		assert.Nil(t, err)
		assert.Equal(t, "tenant1", s.Tenant)

	})
	t.Run("delete", func(t *testing.T) {
		err := store.Delete(sessionID)
		assert.Nil(t, err)
		_, err = store.ById(sessionID)
		assert.NotNil(t, err)
	})

}

func lookup(store SessionStore, id string) func(*testing.T) {
	return func(t *testing.T) {
		sess, err := store.ById(id)
		assert.Nil(t, err)
		assert.Equal(t, id, sess.Id)
	}
}
