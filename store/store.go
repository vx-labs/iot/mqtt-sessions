package store

import (
	"fmt"

	"github.com/hashicorp/go-memdb"
	"github.com/vx-labs/iot-mqtt-events"
	"github.com/vx-labs/iot-mqtt-sessions/errors"
)

const (
	defaultTenant = "_default"
)

type SessionStore interface {
	ById(id string) (events.Session, error)
	All() (events.SessionList, error)
	Exists(id string) bool
	Upsert(sess events.Session) error
	Update(id string, transform func(events.Session) (events.Session, error)) error
	Delete(id string) error
}

type MemDBStore struct {
	state *memdb.MemDB
}

func NewSessionStore() SessionStore {
	db, err := memdb.NewMemDB(&memdb.DBSchema{
		Tables: map[string]*memdb.TableSchema{
			"sessions": {
				Name: "sessions",
				Indexes: map[string]*memdb.IndexSchema{
					"id": {
						Name: "id",
						Indexer: &memdb.StringFieldIndex{
							Field: "Id",
						},
						Unique:       true,
						AllowMissing: false,
					},
					"tenant": {
						Name: "tenant",
						Indexer: &memdb.StringFieldIndex{
							Field: "Tenant",
						},
						Unique:       false,
						AllowMissing: false,
					},
				},
			},
		},
	})
	if err != nil {
		panic(err)
	}
	return &MemDBStore{
		state: db,
	}
}

func (s *MemDBStore) Exists(id string) bool {
	_, err := s.ById(id)
	return err == nil
}
func (s *MemDBStore) BySubscription(topic []byte) (events.SessionList, error) {
	return nil, fmt.Errorf("not implemented")
}
func (s *MemDBStore) ById(id string) (events.Session, error) {
	var session events.Session
	return session, s.read(func(tx *memdb.Txn) error {
		sess, err := s.first(tx, "id", id)
		if err != nil {
			return err
		}
		session = sess
		return nil
	})
}
func (s *MemDBStore) All() (events.SessionList, error) {
	var sessionList events.SessionList
	return sessionList, s.read(func(tx *memdb.Txn) error {
		iterator, err := tx.Get("sessions", "id")
		if err != nil || iterator == nil {
			return errors.SessionNotFound
		}
		for {
			payload := iterator.Next()
			if payload == nil {
				return nil
			}
			sessionList = append(sessionList, payload.(events.Session))
		}
	})
}

func (s *MemDBStore) Upsert(sess events.Session) error {
	if sess.Tenant == "" {
		sess.Tenant = defaultTenant
	}
	return s.write(func(tx *memdb.Txn) error {
		return tx.Insert("sessions", sess)
	})
}
func (s *MemDBStore) Update(id string, transform func(events.Session) (events.Session, error)) error {
	return s.write(func(tx *memdb.Txn) error {
		return s.update(tx, "id", id, transform)
	})
}

func (s *MemDBStore) Delete(id string) error {
	return s.write(func(tx *memdb.Txn) error {
		return tx.Delete("sessions", &events.Session{Id: id})
	})
}

func (s *MemDBStore) read(statement func(tx *memdb.Txn) error) error {
	tx := s.state.Txn(false)
	return s.run(tx, statement)
}
func (s *MemDBStore) write(statement func(tx *memdb.Txn) error) error {
	tx := s.state.Txn(true)
	return s.run(tx, statement)
}
func (s *MemDBStore) run(tx *memdb.Txn, statement func(tx *memdb.Txn) error) error {
	defer tx.Abort()
	err := statement(tx)
	if err != nil {
		return err
	}
	tx.Commit()
	return nil
}

func (s *MemDBStore) update(tx *memdb.Txn, idx, id string, updater func(session events.Session) (events.Session, error)) error {
	iterator, err := tx.Get("sessions", idx, id)
	if err != nil {
		return err
	}
	for {
		payload := iterator.Next()
		if payload == nil {
			return nil
		}
		sess, err := updater(payload.(events.Session))
		if err != nil {
			return err
		}
		tx.Insert("sessions", sess)
	}
}
func (s *MemDBStore) first(tx *memdb.Txn, idx, id string) (events.Session, error) {
	data, err := tx.First("sessions", idx, id)
	if err != nil || data == nil {
		return events.Session{}, errors.SessionNotFound
	}
	return data.(events.Session), nil
}
