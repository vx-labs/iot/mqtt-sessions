package main

import (
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/vx-labs/iot-mqtt-events"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/grpc-ecosystem/grpc-opentracing/go/otgrpc"

	"github.com/sirupsen/logrus"
	"github.com/vx-labs/iot-mqtt-sessions/errors"
	"github.com/vx-labs/iot-mqtt-sessions/state"
	"github.com/vx-labs/iot-mqtt-sessions/tracing"
	"github.com/vx-labs/iot-mqtt-sessions/types"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type StateProvider interface {
	io.Closer
	Store(ctx context.Context, req *types.CreateSessionRequest) error
	Get(ctx context.Context, sessionId string) (events.Session, error)
	All(ctx context.Context) (events.SessionList, error)
	Delete(ctx context.Context, sessionId string) error
	Status() types.HealthCheckResponse_ServingStatus
	UpdateSessionTTL(ctx context.Context, id string) error
	UpdateSessionOnlineStatus(ctx context.Context, id string, status bool) error
}
type SessionStorer struct {
	state StateProvider
}

func main() {
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	port := ":7996"
	lis, err := net.Listen("tcp", port)
	if err != nil {
		logrus.Fatalf("failed to listen: %v", err)
	}
	tracer := tracing.Instance()
	s := grpc.NewServer(
		grpc.UnaryInterceptor(
			otgrpc.OpenTracingServerInterceptor(tracer),
		),
		grpc.StreamInterceptor(
			otgrpc.OpenTracingStreamServerInterceptor(tracer),
		),
	)
	store := &SessionStorer{}
	switch os.Getenv("BACKEND") {
	case "nats":
		store.state = state.NATS(os.Getenv("NATS_URL"), "events")
	default:
		panic("unknown state backend")
	}
	go store.ServeHTTPHealth()
	types.RegisterSessionsServiceServer(s, store)
	quit := make(chan struct{})
	go func() {
		for {
			<-sigc
			s.Stop()
			lis.Close()
			log.Println("starting server shutdown")
			log.Println("shutting down tracing")
			tracing.Close()
			log.Println("tracing shutdown completed")
			log.Println("shutting down sessions server")
			store.state.Close()
			log.Println("sessions server shutdown completed")
			log.Println("server shutdown completed")
			close(quit)
			return
		}
	}()
	logrus.Infof("serving session store on %v", port)
	if err := s.Serve(lis); err != nil {
		logrus.Printf("WARN: server stopped: %v", err)
	}
	<-quit
}
func (s *SessionStorer) ServeHTTPHealth() {
	mux := http.NewServeMux()
	mux.HandleFunc("/health", func(w http.ResponseWriter, _ *http.Request) {
		nodestatus := s.state.Status()
		switch nodestatus {
		case types.HealthCheckResponse_NOT_SERVING:
			w.WriteHeader(http.StatusTooManyRequests)
		case types.HealthCheckResponse_SERVING:
			w.WriteHeader(http.StatusOK)
		default:
			w.WriteHeader(http.StatusServiceUnavailable)
		}
	})
	log.Println(http.ListenAndServe("[::]:9000", mux))
}

func (s *SessionStorer) New(ctx context.Context, in *types.CreateSessionRequest) (*types.SessionUpdated, error) {
	err := s.state.Store(ctx, in)
	if err != nil {
		return nil, err
	}
	logrus.Infof("session %s created", in.Id)
	return &types.SessionUpdated{}, nil
}

func (s *SessionStorer) Get(ctx context.Context, in *events.SessionSelector) (*events.Session, error) {
	sess, err := s.state.Get(ctx, in.Id)
	if err != nil {
		if err == errors.SessionNotFound {
			return nil, status.Error(codes.NotFound, "session not found")
		}
		return nil, err
	}
	if sess.Online {
		sess.Online = events.HasExpired(time.Now().Unix())(sess)
	}
	return &sess, nil
}

func (s *SessionStorer) Delete(ctx context.Context, in *events.SessionSelector) (*types.SessionUpdated, error) {
	return &types.SessionUpdated{}, s.applySelector(ctx, in, func(session events.Session) error {
		err := s.state.Delete(ctx, session.Id)
		if err != nil {
			logrus.Errorf("failed to delete session %s: %v", session.Id, err)
			return err
		} else {
			logrus.Infof("session %s deleted", session.Id)
			return nil
		}
	})
}
func (s *SessionStorer) SetOffline(ctx context.Context, in *events.SessionSelector) (*types.SessionUpdated, error) {
	return &types.SessionUpdated{}, s.applySelector(ctx, in, func(session events.Session) error {
		return s.state.UpdateSessionOnlineStatus(ctx, session.Id, false)
	})
}
func (s *SessionStorer) UpdateTTL(ctx context.Context, in *events.SessionSelector) (*types.SessionUpdated, error) {
	return &types.SessionUpdated{}, s.applySelector(ctx, in, func(session events.Session) error {
		log.Printf("INFO: updated session %s TTL", session.Id)
		return s.state.UpdateSessionTTL(ctx, session.Id)
	})
}

func (s *SessionStorer) applySelector(ctx context.Context, selector *events.SessionSelector, fn func(s events.Session) error) error {
	var (
		set events.SessionList
		err error
	)
	if selector.Id != "" {
		elt, err := s.state.Get(ctx, selector.Id)
		if err != nil {
			return err
		}
		set = append(set, elt)
	} else if len(selector.IdIn) > 0 {
		set = make(events.SessionList, 0, len(selector.IdIn))
		for _, id := range selector.IdIn {
			elt, err := s.state.Get(ctx, id)
			if err != nil {
				continue
			}
			set = append(set, elt)
		}
	} else {
		set, err = s.state.All(ctx)
		if err != nil {
			return err
		}
	}
	now := time.Now().Unix()
	return set.Map(func(sess events.Session) events.Session {
		if sess.Online {
			sess.Online = events.HasExpired(now)(sess)
		}
		return sess
	}).Filter(
		selector.ToFilter()...,
	).ApplyE(fn)
}

func (s *SessionStorer) List(in *events.SessionSelector, stream types.SessionsService_ListServer) error {
	ctx := stream.Context()
	return s.applySelector(ctx, in, func(s events.Session) error {
		return stream.Send(&s)
	})
}
func (t *SessionStorer) Check(ctx context.Context, in *types.HealthCheckRequest) (*types.HealthCheckResponse, error) {
	nodestatus := t.state.Status()
	return &types.HealthCheckResponse{Status: nodestatus}, nil
}
