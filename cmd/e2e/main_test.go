package e2e

import (
	"context"
	"log"
	"os"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/vx-labs/iot-mqtt-sessions/api"
	"github.com/vx-labs/mqtt-protocol/packet"
	"google.golang.org/grpc"
)

type UuidGenerator struct {
	items []string
}

func (u *UuidGenerator) Get() string {
	id := uuid.New().String()
	u.items = append(u.items, id)
	return id
}

func TestE2e(t *testing.T) {
	if os.Getenv("RUN_E2E_TESTS") == "" {
		t.Skip("skipping test - $RUN_E2E_TESTS not set")
	}
	conn, err := grpc.Dial("127.0.0.1:7996", grpc.WithInsecure())
	if err != nil {
		log.Fatalln(err)
	}
	client := api.New(conn)
	ctx := context.Background()
	generator := UuidGenerator{items: []string{}}

	t.Run("should be able to create sessions", func(t *testing.T) {
		connect := &packet.Connect{}
		assert.Nil(t, client.New(ctx, generator.Get(), "_default", connect))
		assert.Nil(t, client.New(ctx, generator.Get(), "_default", connect))
		assert.Nil(t, client.New(ctx, generator.Get(), "_default", connect))
		assert.Nil(t, client.New(ctx, generator.Get(), "_default", connect))
		assert.Nil(t, client.New(ctx, generator.Get(), "_default", connect))
	})

}

func BenchmarkE2e(b *testing.B) {
	if os.Getenv("RUN_E2E_TESTS") == "" {
		b.Skip("skipping test - $RUN_E2E_TESTS not set")
	}
	conn, err := grpc.Dial("127.0.0.1:7996", grpc.WithInsecure())
	if err != nil {
		log.Fatalln(err)
	}
	client := api.New(conn)

	ctx := context.Background()
	generator := UuidGenerator{items: []string{}}

	b.Run("create", func(b *testing.B) {
		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				err := client.New(ctx, generator.Get(), "_default", &packet.Connect{})
				if err != nil {
					b.Fatal(err)
				}
			}
		})
	})
	b.Run("lookup", func(b *testing.B) {
		id := generator.items[0]
		for i := 0; i < b.N; i++ {
			sess, err := client.Get(ctx, id)
			if err != nil {
				b.Fatal(err)
			}
			if sess.Id != id {
				b.Fatal("wrong session fetched")
			}
		}
	})
}
