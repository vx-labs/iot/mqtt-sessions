package commands

import (
	"context"
	"fmt"
	"log"

	"github.com/vx-labs/iot-mqtt-events"

	"text/template"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/vx-labs/iot-mqtt-sessions/api"
	"github.com/vx-labs/mqtt-protocol/packet"
	"google.golang.org/grpc"
)

type EndpointProvider interface {
	Endpoint(service string) (endpoint string, token string)
}

func getClient(helper EndpointProvider) *api.Client {
	endpoint, _ := helper.Endpoint("queues")
	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	client := api.New(conn)
	return client
}

func creator(ctx context.Context, helper EndpointProvider) *cobra.Command {
	c := &cobra.Command{
		Use: "create",
		Run: func(cmd *cobra.Command, _ []string) {
			client := getClient(helper)
			id, err := cmd.Flags().GetString("id")
			if err != nil {
				log.Fatalf("Error: %v", err)
				return
			}
			if id == "" {
				log.Fatalln("topic argument is required")
			}
			tenant, err := cmd.Flags().GetString("tenant")
			if err != nil {
				log.Fatalf("Error: %v", err)
			}
			if tenant == "" {
				log.Fatalln("tenant argument is required")
			}
			err = client.New(ctx, id, tenant, &packet.Connect{
				KeepaliveTimer: 30,
			})
			if err != nil {
				log.Fatalln(err)
			}
		},
	}
	c.Flags().StringP("id", "i", "", "use given session id")
	c.Flags().StringP("tenant", "t", "_default", "use given tenant")
	return c
}
func delete(ctx context.Context, helper EndpointProvider) *cobra.Command {
	c := &cobra.Command{
		Use:  "delete",
		Args: cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, ids []string) {
			client := getClient(helper)
			for _, id := range ids {
				err := client.Delete(ctx, id)
				if err != nil {
					log.Printf("WARN: failed to delete ID %s: %v", id, err)
				} else {
					fmt.Println(id)
				}
			}
		},
	}
	return c
}
func refresh(ctx context.Context, helper EndpointProvider) *cobra.Command {
	c := &cobra.Command{
		Use:  "refresh",
		Args: cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, ids []string) {
			client := getClient(helper)
			for _, id := range ids {
				err := client.UpdateTTL(ctx, id)
				if err != nil {
					log.Printf("WARN: failed to refresh TTL for %s: %v", id, err)
				} else {
					fmt.Println(id)
				}
			}
		},
	}
	return c
}
func ToString(b []byte) string {
	return string(b)
}
func list(ctx context.Context, helper EndpointProvider) *cobra.Command {
	sessionTemplate := `• {{ .Id | green | bold }}
  {{ "Online:"       | faint }} {{ .Online }}
  {{ "Tenant:"       | faint }} {{ .Tenant }}
  {{ "Clean:"       | faint }} {{ .Clean }}
  {{ "Username:"       | faint }} {{ .Username  | toString }}
  {{ "Client ID:"       | faint }} {{ .ClientID | toString }}
  {{ "Deadline:"       | faint }} {{ .Deadline }}
  {{ "Keepalive interval:"       | faint }} {{ .KeepaliveInterval }}s`
	tpl, err := template.New("").Funcs(promptui.FuncMap).Funcs(template.FuncMap{"toString": ToString}).Parse(fmt.Sprintf("%s\n", sessionTemplate))
	if err != nil {
		panic(err)
	}
	c := &cobra.Command{
		Use: "list",
		Run: func(cmd *cobra.Command, _ []string) {
			client := getClient(helper)
			tenant, err := cmd.Flags().GetString("tenant")
			if err != nil {
				log.Fatalf("Error: %v", err)
			}
			if tenant == "" {
				log.Fatalln("tenant argument is required")
			}
			ids, err := cmd.Flags().GetStringArray("id")
			if err != nil {
				log.Fatalf("Error: %v", err)
			}

			ctx := context.Background()
			set, err := client.List(ctx,
				api.HasTenant(tenant),
				api.HasIDIn(ids),
			)
			if err != nil {
				log.Fatalln(err)
			}
			set.Apply(func(sess events.Session) {
				tpl.Execute(cmd.OutOrStdout(), sess)
			})
		},
	}
	c.Flags().StringP("tenant", "t", "_default", "select given tenant")
	c.Flags().StringArrayP("id", "i", nil, "select given ids")
	return c
}
func Sessions(ctx context.Context, helper EndpointProvider) *cobra.Command {
	app := &cobra.Command{
		Use:   "sessions",
		Short: "manage session",
	}
	app.AddCommand(creator(ctx, helper))
	app.AddCommand(delete(ctx, helper))
	app.AddCommand(refresh(ctx, helper))
	app.AddCommand(list(ctx, helper))
	return app
}
